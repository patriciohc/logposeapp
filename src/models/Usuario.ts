export class Usuario  {
    id?: string;
    nombre: string;
    apellidos: string;
    correo_electronico: string;
    telefono: string;
    password?: string;
}