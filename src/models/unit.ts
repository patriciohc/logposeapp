export interface Unit {
    id: string;
    id_usuario: string;
    socio: any;
    direccion: string;
    logo: string;
    telefono: string;
}