export interface Catalog {
    id: string;
    nombre: string;
    descripcion?: string;
}