import React, { useEffect } from "react";
import { NavigationContainer, DrawerActionHelpers } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Icon } from "react-native-elements";
import AccountStack from './AccountStack';

const Drawer = createDrawerNavigator();

export default () => {

    useEffect( () => {

    }, [])
    return (
        <NavigationContainer>
            <Drawer.Navigator initialRouteName="Home">
              <Drawer.Screen name="Home" component={AccountStack} />
            </Drawer.Navigator>
        </NavigationContainer>
    )
}