import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "../pages/Login";
import Register from "../pages/Register";
import { Button } from "react-native-elements";
import { Icon } from "react-native-elements";
import { View } from "react-native";

const Stack = createStackNavigator();


export default () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="login"
                component={Login}
                options={({ navigation, route }) => ({ 
                    title: "Iniciar sesión",
                    headerLeft: () => (
                        <View style={{padding: 10}}>
                            <Icon onPress={()=>{navigation.toggleDrawer();}} name="menu" size={22} color={"#646464"}/>
                        </View>
                    ),
                })}
            />
            <Stack.Screen
                name="register"
                component={Register}
                options={{ title: "Registro" }}
            />
        </Stack.Navigator>
    );
}
