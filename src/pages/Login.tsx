import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Input, Icon, Button, Image } from "react-native-elements";
import { isEmpty } from "lodash";
import { useNavigation } from "@react-navigation/native";
import { useSelector, useDispatch } from 'react-redux';
import Loading from "../components/Loading";
import { ScrollView } from "react-native-gesture-handler";
import { loadRefreshToken } from "../actions/token.actions";

export default (props) => {

    const { toastRef } = props;
    const [showPassword, setShowPassword] = useState(false);
    const [formData, setFormData] = useState(defaultFormValue());
    // const navigation = useNavigation();

    const dispatch = useDispatch();

    const onChange = (e, type) => {
        setFormData({ ...formData, [type]: e.nativeEvent.text });
    };

    const onSubmit = () => {
        if (isEmpty(formData.email) || isEmpty(formData.password)) {
            // toastRef.current.show("Todos los campos son obligatorios");
        } else {
            dispatch(loadRefreshToken(formData.email, formData.password));
        }
    };

    return (
        <ScrollView>
            <Image
                source={require("../../assets/img/logo_1.png")}
                resizeMode="contain"
                style={styles.logo}/>
            <View style={styles.formContainer}>
                <Input
                    placeholder="Correo electronico"
                    containerStyle={styles.inputForm}
                    onChange={(e) => onChange(e, "email")}
                    rightIcon={
                        <Icon
                            type="material-community"
                            name="at"
                            iconStyle={styles.iconRight}
                        />
                    }
                />
                <Input
                    placeholder="Contraseña"
                    containerStyle={styles.inputForm}
                    secureTextEntry={showPassword ? false : true}
                    onChange={(e) => onChange(e, "password")}
                    rightIcon={
                        <Icon
                            type="material-community"
                            name={showPassword ? "eye-off-outline" : "eye-outline"}
                            iconStyle={styles.iconRight}
                            onPress={() => setShowPassword(!showPassword)}
                        />
                    }
                />
                <Button
                    title="Iniciar sesión"
                    containerStyle={styles.btnContainerLogin}
                    buttonStyle={styles.btnLogin}
                    onPress={onSubmit}
                />
                <CreateAccount/>
                <Loading isVisible={false} text="Iniciando sesión" />
            </View>
        </ScrollView>
    );
}


function CreateAccount () {
    const navigation = useNavigation();
    return (
        <Text style={styles.textRegister}>
            ¿Aun no tienes una cuenta?{" "}
            <Text onPress={() => navigation.navigate("register")} style={styles.btnRegister}>
                Registrate
            </Text>
        </Text>
    );
}


function defaultFormValue() {
    return {
        email: "",
        password: "",
    };
}

const styles = StyleSheet.create({
    logo: {
        width: "100%",
        height: 150,
        marginTop: 30
    },
    formContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
    },
    inputForm: {
        width: "100%",
        marginTop: 20,
    },
    btnContainerLogin: {
        marginTop: 20,
        width: "95%",
    },
    btnLogin: {
        backgroundColor: "#5F678E",
    },
    iconRight: {
        color: "#c1c1c1",
    },
    textRegister: {
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10
    },
    btnRegister: {
        color: "#5F678E",
        fontWeight: "bold"
    }
});