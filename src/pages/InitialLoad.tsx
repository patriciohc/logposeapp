import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { loadCategories } from '../actions/categories.actions';
import { setToken } from '../actions/token.actions';
import { AppState } from './../reducers/index';
import { Alert, ActivityIndicator, StyleSheet, View, Modal } from 'react-native';
import Loading from '../components/Loading';
import firebase from 'firebase';



export default () => {

    const isLoading = useSelector((state: AppState) => state.stateCategories.loading);
    const error = useSelector((state: AppState) => state.stateCategories.error);
    const token  = useSelector((state: AppState) => state.stateToken.token);
    const refreshToken = useSelector((state: AppState) => state.stateToken.refreshToken);
    const tokenError = useSelector((state: AppState) => state.stateToken.error);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadCategories());

        firebase.auth().onAuthStateChanged(async function(user) {
            console.log("change auth", user);
            if (user) {
                //const token = await user.getIdToken();
                //dispatch(setToken(token));
                //console.log("Token: " + token);
            }
        });

    }, []);

    useEffect(() => {
        console.log('get refresh token', refreshToken);
        if (refreshToken) {
            firebase.auth().signInWithCustomToken(refreshToken)
                .catch(error => {
                    console.log('Ocurrio un error al autenticarse', error);
                });
        }
    }, [refreshToken]);

    useEffect(() => {
        console.log('Ocurrio un error al autenticarse', tokenError);
    }, [tokenError]);

    const createAlertErrorMessage = (error: string) =>
        Alert.alert(
            "Ocurrio un error al cargar los datos",
            error,
            [
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
        );

    return (
        <View>           
            <Loading isVisible={false} text="cargando.."/>
            {error &&
                createAlertErrorMessage(error.toString())
            }         
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
});