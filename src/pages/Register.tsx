import React, { useState, useRef, useEffect } from "react";
import { StyleSheet, View, Text, Alert } from "react-native";
import { Input, Icon, Button, Image } from "react-native-elements";
import { isEmpty } from "lodash";
import { useNavigation } from "@react-navigation/native";
import Loading from "../components/Loading";
import { ScrollView } from "react-native-gesture-handler";
import Toast from "react-native-easy-toast";
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from "../reducers";
import { createUser, cleanUser } from "../actions/usuario.actions";
import { Usuario } from "../models/Usuario";
import { loadRefreshToken } from "../actions/token.actions";

export default () => {

    const toastRef: React.MutableRefObject<Toast> = useRef();

    const [showPassword, setShowPassword] = useState(false);
    const [formData, setFormData] = useState(defaultFormValue());
    const navigation = useNavigation();
    const usuario = useSelector((state: AppState) => state.stateUsuario.usuario);
    const error = useSelector((state: AppState) => state.stateUsuario.error);
    const isLoading = useSelector((state: AppState) => state.stateUsuario.loading);
    
    const dispatch = useDispatch();

    const onChange = (e, type) => {
        setFormData({ ...formData, [type]: e.nativeEvent.text });
    };

    const onSubmit = () => {
        var msg = getMsgValidation(formData);
        if (!msg) {
            dispatch(createUser(formData));        
        } else {
            toastRef.current.show(msg);
        }
    };

    useEffect(() => {
        console.log('new user: ', usuario);
        if (usuario) {
            const username = formData.correo_electronico;
            const password = formData.password;
            dispatch(loadRefreshToken(username, password));
        }
    }, [usuario]);

    const createAlertErrorMessage = (error: string) =>
        Alert.alert(
            "Ocurrio un error al registrar el usuario",
            error,
            [
                { text: "OK", onPress: () => dispatch(cleanUser()) }
            ]
        );

    return (
        <ScrollView>
            <View style={styles.formContainer}>
                <Input
                    placeholder="Nombre"
                    containerStyle={styles.inputForm}
                    onChange={(e) => onChange(e, "nombre")}
                    rightIcon={
                        <Icon
                            type="material-community"
                            name="account"
                            iconStyle={styles.iconRight}
                        />
                    }
                />
                <Input
                    placeholder="Apellidos"
                    containerStyle={styles.inputForm}
                    onChange={(e) => onChange(e, "apellidos")}
                    rightIcon={
                        <Icon
                            type="material-community"
                            name="account"
                            iconStyle={styles.iconRight}
                        />
                    }
                />
                <Input
                    placeholder="Correo electronico"
                    containerStyle={styles.inputForm}
                    keyboardType="email-address"
                    onChange={(e) => onChange(e, "correo_electronico")}
                    rightIcon={
                        <Icon
                            type="material-community"
                            name="at"
                            iconStyle={styles.iconRight}
                        />
                    }
                />
                <Input
                    placeholder="Telefono"
                    containerStyle={styles.inputForm}
                    keyboardType="numeric"
                    onChange={(e) => onChange(e, "telefono")}
                    rightIcon={
                        <Icon
                            type="material-community"
                            name="phone"
                            iconStyle={styles.iconRight}
                        />
                    }
                />
                <Input
                    placeholder="Contraseña"
                    containerStyle={styles.inputForm}
                    secureTextEntry={showPassword ? false : true}
                    onChange={(e) => onChange(e, "password")}
                    rightIcon={
                        <Icon
                            type="material-community"
                            name={showPassword ? "eye-off-outline" : "eye-outline"}
                            iconStyle={styles.iconRight}
                            onPress={() => setShowPassword(!showPassword)}
                        />
                    }
                />
                <Button
                    title="Registrarse"
                    containerStyle={styles.btnContainerLogin}
                    buttonStyle={styles.btnLogin}
                    onPress={onSubmit}
                />
                <Loading isVisible={false} text="Iniciando sesión" />
            </View>
            <Toast
                ref={toastRef}
            />

            <Loading isVisible={isLoading} text="cargando.."/>
            {error &&
                createAlertErrorMessage(error.toString())
            }
        </ScrollView>
    );

    
}


function getMsgValidation(value: Usuario) {
    if (!value) {
        return 'Los campos son requeridos';
    }

    for (let key in value) {
        if (value[key].length < 3) {
            return key + ' no valido'
        }
    }

    if (value.telefono.length != 10) {
        return 'Telefono no valido'
    }

    if (value.password.length < 6) {
        return 'La constraseña debe tener almenos 6 caracterres'
    }

    var regExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g
    if (!regExp.test(value.correo_electronico)) {
        return 'Correo electronico no valido'
    }

    return null;
}

function defaultFormValue(): Usuario {
    return {
        nombre: "",
        apellidos: "",
        correo_electronico: "",
        telefono: "",
        password: "",
    };
}

const styles = StyleSheet.create({
    logo: {
        width: "100%",
        height: 150,
        marginTop: 30
    },
    formContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
    },
    inputForm: {
        width: "100%",
        marginTop: 20,
    },
    btnContainerLogin: {
        marginTop: 20,
        width: "95%",
    },
    btnLogin: {
        backgroundColor: "#5F678E",
    },
    iconRight: {
        color: "#c1c1c1",
    },
    textRegister: {
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10
    },
    btnRegister: {
        color: "#5F678E",
        fontWeight: "bold"
    }
});