import React, { useState} from 'react';
import { View, Text, Picker, TextInput } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../reducers';
import { selectCategory } from '../actions/categories.actions';

export default () => {

    const categorySelected = useSelector((state: AppState ) => state.stateCategories.selected);
    const categories = useSelector((state: AppState ) => state.stateCategories.list);
    const dispatch = useDispatch();
    
    return (
        <View
            style={{
                flexDirection: 'column'
            }}>
            <Picker
                selectedValue={categorySelected} 
                onValueChange={
                    (itemValue, itemIndex) => dispatch(selectCategory(itemValue))
                }>
                {categories.map(item => (<Picker.Item key={item.id} label={item.nombre} value={item.id} />))}
            </Picker>
        </View>
    );

}


