// import React, { Component } from 'react';
// import { View, Text, TextInput, ProgressBarAndroid } from 'react-native';
// import { ListItem } from 'react-native-elements'
// import { AppState } from '../reducers';
// import { Unit } from '../models/unit';
// import { connect } from 'react-redux';
// import { searchUnits } from '../actions/units.actions';
// import { BUCKET } from '../constanst/share';

// interface ListUnitsType {
//     searchUnits: Function;
//     units: Array<Unit>;
//     navigation: any;
//     unitsIsLoading: boolean;
//     categorySelected: string;
// }

// class ListUnits extends Component<ListUnitsType> {

//   componentDidMount() {
//     this.props.searchUnits();
//   }

//   componentDidUpdate(prevProps) {
//     if (prevProps.categorySelected !== this.props.categorySelected) {
//       this.props.searchUnits({categoriaId: this.props.categorySelected})
//     }
//   }

//   render() {
//     return (
//       <View>
//         { this.props.unitsIsLoading ? <ProgressBarAndroid /> :
//           this.props.units.map((unit, i) => (
//             <ListItem
//               key={unit.id}
//               leftAvatar={{ source: { uri: unit.socio ? (BUCKET + '/' + unit.socio.id_usuario + '%2Fsocio%2Flogo.jpg?alt=media') : 'https://firebasestorage.googleapis.com/v0/b/logposeapp.appspot.com/o/1778ff24-e53c-44f0-804c-e2a51ac50fd2%2Fsocio%2Flogo.jpg?alt=media' } }}
//               title={unit.socio ? unit.socio.nombre_empresa : 'No disponible'}
//               subtitle={unit.direccion + '\n tel: ' + unit.telefono}
//               bottomDivider
//             />
//           ))
//         }
//       </View>
//     );
//   }
// }

// const mapStateToProps = (state: AppState) => ({
//     units: state.stateUnits.list,
//     unitsIsLoading: state.stateUnits.loading,
//     categorySelected: state.stateCategories.selected
// })

// const mapDispatchToProps = {
//     searchUnits
// }

// export default connect(mapStateToProps, mapDispatchToProps)(ListUnits);