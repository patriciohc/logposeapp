import { Action } from '../models/Action';
import { Usuario } from '../models/Usuario';

export const CREATE_USER = 'CREATE_USER';
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS';
export const CREATE_USER_FAIL = 'CREATE_USER_FAIL';
export const CLEAN_USER = 'CLEAN_USER';

export function createUser(usuario: Usuario): Action {
    return {
        type: CREATE_USER,
        payload: {
            request: {
                method: 'post',
                url: '/usuario',
                data: usuario
            }
        }
    }
};


export function cleanUser(): Action {
    return {
        type: CLEAN_USER
    }
};