import { Action } from '../models/Action';
import { Catalog } from '../models/catalog';

export const LOAD_CATEGORIES = 'LOAD_CATEGORIES';
export const LOAD_CATEGORIES_SUCCESS = 'LOAD_CATEGORIES_SUCCESS';
export const LOAD_CATEGORIES_FAIL = 'LOAD_CATEGORIES_FAIL';
export const SELECT_CATEGORY = 'SELECT_CATEGORY';

export function loadCategories(): Action {
    return {
        type: LOAD_CATEGORIES,
        payload: {
            request: {
                url: '/categoria-socio'
            }
        }
    }
};

export function selectCategory(categoryId: string): Action {
    return {
        type: SELECT_CATEGORY,
        payload: { categoryId }
    }
};