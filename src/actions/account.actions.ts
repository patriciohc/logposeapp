import { Action } from '../models/Action';
import { Catalog } from '../models/catalog';

export const LOAD_CATEGORIES = 'LOAD_ACCOUNT';
export const LOAD_CATEGORIES_SUCCESS = 'LOAD_ACCOUNT_SUCCESS';
export const LOAD_CATEGORIES_FAIL = 'LOAD_ACCOUNT_FAIL';
export const SELECT_CATEGORY = 'SELECT_CATEGORY';

export function loadCategories(): Action {
    return {
        type: LOAD_CATEGORIES,
        payload: {
            request: {
                url: '/categoria-socio'
            }
        }
    }
};

export function selectCategory(categoryId: string): Action {
    return {
        type: SELECT_CATEGORY,
        payload: { categoryId }
    }
};