import { Action } from '../models/Action';


export const LOAD_REFRESHTOKEN = 'LOAD_REFRESHTOKEN';
export const LOAD_REFRESHTOKEN_SUCCESS = 'LOAD_REFRESHTOKEN_SUCCESS';
export const LOAD_REFRESHTOKEN_FAIL = 'LOAD_REFRESHTOKEN_FAIL';

export const SET_TOKEN = 'SET_TOKEN';


export function loadRefreshToken(username: string, password: string): Action {
    return {
        type: LOAD_REFRESHTOKEN,
        payload: { 
            request: {
                url: '/account/tokenrefresh?tipo=comprador',
                auth: { username, password }
            }
        }
    }
};

export function setToken(token: string): Action {
    return {
        type: SET_TOKEN,
        payload: { token }
    }
};