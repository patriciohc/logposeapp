import { Action } from '../models/Action';
import { Unit } from '../models/unit';

export const SEARCH_UNITS = 'LOAD_UNITS';
export const SEARCH_UNITS_SUCCESS = 'LOAD_UNITS_SUCCESS';
export const SEARCH_UNITS_FAIL = 'LOAD_UNITS_FAIL'
export const SELECT_UNIT = 'SELECT_UNIT'

export interface QueryUnits {
    lat?: number;
    lng?: number;
    categoriaId?: string;
    texto?: string;
}

export function searchUnits(queryUnits: QueryUnits = {}): Action {

    let queryItems = [];
    for (let key in queryUnits) {
        if (queryUnits[key]) {
            queryItems.push(key+'='+queryUnits[key])
        }
    }

    let url = '/unidad/public' + (queryItems.length ? '?' + queryItems.join('&') : '');

    return {
        type: SEARCH_UNITS,
        payload: {
            request: { url }
        }
    }
};

export function selectUnit(unit: Unit): Action {
    return {
        type: SELECT_UNIT,
        payload: {
            unit: unit 
        }
    }
};