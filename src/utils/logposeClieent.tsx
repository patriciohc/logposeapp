import axios from 'axios';
import { API } from '../constanst/share';

export default axios.create({
    baseURL: API,
    responseType: 'json'
});
