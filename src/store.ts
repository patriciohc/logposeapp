import reducers from './reducers';
import axiosMiddleware from 'redux-axios-middleware';
import { createStore, applyMiddleware } from 'redux';
import client from './utils/logposeClieent';
import { composeWithDevTools } from 'redux-devtools-extension';


export default createStore(
    reducers,
    composeWithDevTools(applyMiddleware(axiosMiddleware(client)))
);