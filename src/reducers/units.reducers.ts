import { Unit } from '../models/unit';
import { Action } from '../models/Action';
import { 
    SEARCH_UNITS,
    SEARCH_UNITS_SUCCESS,
    SEARCH_UNITS_FAIL,
    SELECT_UNIT
} from '../actions/units.actions';

export class StateUnits {
    loading: boolean;
    list: Array<Unit>;
    error: null;
    selected: string;
}


const INITIAL_STATE: StateUnits = {
    loading: false,
    error: null,
    list: [],
    selected: null
}

export const unitsReducer = (state: StateUnits = INITIAL_STATE, action: Action) => {
    switch (action.type) {
        case SEARCH_UNITS:
            return { ...state, loading: true };
        case SEARCH_UNITS_SUCCESS:
            return { ...state, loading: false, list: action.payload.data, error: null };
        case SEARCH_UNITS_FAIL:
            console.error(action.error);
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case SELECT_UNIT:
            return { ...state, selected: action.payload.unitId };
        default:
            return state;
    }
};