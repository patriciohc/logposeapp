import { Catalog } from '../models/catalog';
import { Action } from '../models/Action';
import { 
    LOAD_REFRESHTOKEN,
    LOAD_REFRESHTOKEN_SUCCESS,
    SET_TOKEN,
    LOAD_REFRESHTOKEN_FAIL
} from '../actions/token.actions';

export class StateToken {
    token: String;
    refreshToken: string;
    isLoading: boolean;
    error: string
}


const INITIAL_STATE: StateToken = {
    token: null,
    refreshToken: null,
    isLoading: false,
    error: null,
}

export const tokenReducer = (state: StateToken = INITIAL_STATE, action: Action) => {
    switch (action.type) {
        case LOAD_REFRESHTOKEN:
            console.log('load refreshtoken', action);
            return {...state, isLoading: true };
        case LOAD_REFRESHTOKEN_SUCCESS:
            console.log('load refreshtoken success' , action);
            return { ...state, isLoading: false, refreshToken: action.payload.data.token };
        case LOAD_REFRESHTOKEN_FAIL:
            console.log('load refreshtoken fail' , action);
            return { ...state, isLoading: false, error: action.payload.error };
        case SET_TOKEN:
            console.log('load set token' , action);
            return { ...state, token: action.payload.token };
        default:
            return state;
    }
};