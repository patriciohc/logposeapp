import { Catalog } from '../models/catalog';
import { Action } from '../models/Action';
import { 
    LOAD_CATEGORIES,
    LOAD_CATEGORIES_SUCCESS,
    LOAD_CATEGORIES_FAIL,
    SELECT_CATEGORY
} from '../actions/categories.actions';

export class StateCategories {
    loading: boolean;
    list: Array<Catalog>;
    error: null;
    selected: string;
}


const INITIAL_STATE: StateCategories = {
    loading: false,
    error: null,
    list: [
        {id: '', nombre: 'Todas las categorias'}
    ],
    selected: ''
}

export const categoriesReducer = (state: StateCategories = INITIAL_STATE, action: Action) => {
    switch (action.type) {
        case LOAD_CATEGORIES:
            return { ...state, loading: true };
        case LOAD_CATEGORIES_SUCCESS:
            return { 
                ...state,
                loading: false,
                list: [{id: '', nombre: 'Todas las categorias'}, ...action.payload.data],
                error: null
            };
        case LOAD_CATEGORIES_FAIL:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case SELECT_CATEGORY:
            return { ...state, selected: action.payload.categoryId };
        default:
            return state;
    }
};