import { Action } from '../models/Action';
import { CREATE_USER_SUCCESS, CREATE_USER_FAIL, CLEAN_USER, CREATE_USER } from '../actions/usuario.actions';
import { Usuario } from '../models/Usuario';

export class StateUsuario {
    loading: boolean;
    usuario: Usuario;
    error: any;
}


const INITIAL_STATE: StateUsuario = {
    loading: false,
    error: null,
    usuario: null
}

export const usuarioReducer = (state: StateUsuario = INITIAL_STATE, action: Action) => {
    switch (action.type) {
        case CREATE_USER:
            return { ...state, loading: true};
        case CREATE_USER_SUCCESS:
            return { ...state, loading: false, usuario: action.payload.data, error: null };
        case CREATE_USER_FAIL:
            return { ...state, loading: false, error: action.error };
        case CLEAN_USER:
            return INITIAL_STATE;
        default:
            return state;
    }
};