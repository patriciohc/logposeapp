import { combineReducers } from 'redux';
import { categoriesReducer, StateCategories } from './categories.reducers';
import { StateUnits, unitsReducer } from './units.reducers';
import { StateToken,  tokenReducer } from './token.reducers';
import { StateUsuario, usuarioReducer } from './usuario.reducers';

export interface AppState {
    stateCategories: StateCategories,
    stateUnits: StateUnits,
    stateToken: StateToken,
    stateUsuario: StateUsuario
}

export default combineReducers({
    stateCategories: categoriesReducer,
    stateUnits: unitsReducer,
    stateToken: tokenReducer,
    stateUsuario: usuarioReducer
});
