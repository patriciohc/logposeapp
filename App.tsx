import React, { useEffect } from 'react';
import { Provider, useDispatch } from 'react-redux';
import InitialLoad from './src/pages/InitialLoad';
import store from './src/store';
import Navigation from './src/navigations/Navigation';
import firebase from 'firebase';
import { firebaseConfig } from './src/utils/firebase';

// Initialize Firebase
export const firebaseApp = firebase.initializeApp(firebaseConfig);

export default () => {
    return (
        <Provider store={store}>
            <InitialLoad/>
            <Navigation/>
        </Provider>
    )
}

/**
 * #D4F2F9
 * #5F678E
 * #3D4264
 */